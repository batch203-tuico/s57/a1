let collection = [];

// Write the queue functions below.
// 1. ==========================================
function print() {
    return collection;
};
// 2, 3, 5, & 6. ==================================
function enqueue(name) {
    collection[collection.length] = name;
    return collection;
};

// 4. =============================================
function dequeue() {
    for (let i = 1; i < collection.length; i++)
        collection[i - 1] = collection[i];
        collection.length--;
        return collection;

};
// 7. =============================================
function front() {
    let element = collection[0];
    return element;
};
// 8. =============================================
function size() {
    let size = collection.length;
    return size;
};
// 8. =============================================
function isEmpty() {
    if(collection.length !== 0){
        return false;
    } else {
        return true;
    }
};

// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};